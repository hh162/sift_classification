from sklearn.mixture import GaussianMixture
import numpy as np

class VfEncoder:
    '''
    INPUT:
    xx  - a (T,D) matrix,where T is # of keypoints and D is the number of dimentison of a keypoint.
    gmm - a pretrained model on all images.

    OUTPUT:
    fv  - the fisher vector encoder return a (2K+1) dimensional vector. The dimension of this output is independent from either the number or the dimension of the keypoints/extractors.
    '''
    def __init__(K=30, X):
        self.K = K
        self.gmm = gmm(n_components=k)
        self.gmm.fit(X)
        self.T = np.size(X, 0)
        self.D = np.size(X, 1)
        return
    def compute(xx):
    # parameters of the pre-trained gmm model
    w = self.gmm.weights_
    u = self.gmm.means_
    sigma = self.gmm.covars_
    
    # statistic parameters
    s0 = np.zeros(k,1)
    s1 = np.zeros(k,1)
    s2 = np.zeros(1,1)
    for t in range(self.T):
        for k in range(self.K):
            s0 += gamma(t,k)
            s1 += gamma(t,k) * xx[t,:]
            s2 += gamma(t,k) * xx[t,:] ** 2
    for k in range(self.K):
        g_a[k] = (s0[k] - self.T * w[k]) / np.sqrt(w[k])
        g_u[k] = (s1[k] - u[k] * s0[k]) / (np.sqrt(w[k]) * sigma[k])
        g_sigma[k] = (s2[k] - 2 * u[k] * s1[k] + (u[k]**2 - sigma[k]**2) * s0[k]) / (np.sqrt(2 * w[k]) * sigma[k]**2)

    # normalization
    return np.hstack([g_a, g_u, g_sigma])
    
