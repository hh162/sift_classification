# Sift classification
-----
This project attempt to classify items using SIFT feature extractor plus a machine learning classifier. Before I try out different algorithms, there are some options I am going to look at:
  - SIFT + SVM
  - SIFT + FV/GMM(Oxford)
